import $ from 'jquery';
import Component from '@ember/component';

export default Component.extend({
  actions: {
    confirmImage(image) {
      $(`#${this.get('modalId')}`).modal('toggle');
      this.get('onConfirm')(image);
    }
  }
});
