import { computed } from '@ember/object';
import moment from 'moment';
import Component from '@ember/component';

export default Component.extend({
    orderedSlides: computed('slides', function() {
        return this.get('slides').sort((left, right) => {
            return moment.utc(left.date).diff(moment.utc(right.date))
        });
    }),
});
