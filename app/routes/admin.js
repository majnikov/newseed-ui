import { inject as service } from '@ember/service';
import Route from '@ember/routing/route';
import AuthenticatedRouteMixin from 'ember-simple-auth/mixins/authenticated-route-mixin';

export default Route.extend(AuthenticatedRouteMixin, {
  session: service(),

  beforeModel(transition) {
    if (!this.get('session').isAuthenticated) {
      this.transitionTo('login');
    }
    if (transition.targetName === "admin.index" && this.get('session').isAuthenticated) {
      this.transitionTo('admin.content');
    }
  },

});
