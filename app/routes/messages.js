import Route from '@ember/routing/route';
import SetChurchController from 'newseed/mixins/org-set-controller';

import { inject as service } from '@ember/service';

import { setQuery } from 'newseed/utils/set-query';

export default Route.extend(SetChurchController, {
  infinity: service(),
  queryParams: {
    speaker: {
      refreshModel: true
    },
    tags: {
      refreshModel: true
    }
  },
  model(params) {
    const fullQuery = setQuery(
      {
        status: 'published',
        perPage: 20,
        startingPage: 1,
        countParam: 'meta.count',
      }, 
      params
    );
    return this.infinity.model('sermon', fullQuery);
  },
  actions: {
    refreshRoute() {
      this.refresh();
    }
  }
});
