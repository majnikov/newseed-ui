import Route from '@ember/routing/route';
import AuthenticatedRouteMixin from 'ember-simple-auth/mixins/authenticated-route-mixin';
import config from 'newseed/config/environment';

export default Route.extend(AuthenticatedRouteMixin, {
  model() {
    return this.store.findRecord('org', config.org);
  }
});
