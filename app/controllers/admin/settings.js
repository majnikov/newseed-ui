import { computed, get, set } from '@ember/object';
import Controller from '@ember/controller';


export default Controller.extend({
  meetingTimesArray: computed('model.meetingTimes', function () {
    const times = get(this, 'model.meetingTimes');
    return times.split(',');
  }),
  actions: {
    saveSettings() {
      // Turn the string to an array if required
      if (!Array.isArray(get(this, 'model.meetingTimes'))) set(this, 'model.meetingTimes', get(this, 'meetingTimesArray'))
      get(this, 'model').save()
        .then(() => {
          this.get('flashMessages').success('Settings succesfully saved');
        })
        .catch(() => {
          this.get('flashMessages').danger('Something went wrong - settings not saved');
        })
    }
  }
});
