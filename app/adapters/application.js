import DS from 'ember-data';
import DataAdapterMixin from 'ember-simple-auth/mixins/data-adapter-mixin';
import config from 'newseed/config/environment';
import { inject as service } from '@ember/service';
import { isEmpty } from '@ember/utils';

const { RESTAdapter } = DS;

export default RESTAdapter.extend(DataAdapterMixin, {
  host: config.apiHost,
  namespace: config.apiNameSpace,

  session: service('session'),

  init() {
    this._super(...arguments);
    this.set('headers', {
      'ORG': config.org
    });
  },

  authorize(xhr) {
    let { access_token } = this.get('session.data.authenticated');
    if (this.get('session.isAuthenticated') && !isEmpty(access_token)) {
      xhr.setRequestHeader('Authorization', `Bearer ${access_token}`);
    }
  },
});
