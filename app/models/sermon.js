import DS from 'ember-data';

const d = new Date();

export default DS.Model.extend({
  title: DS.attr('string'),
  date: DS.attr('date', {
    defaultValue() {
      return d;
    }
  }),
  text: DS.attr('string', { defaultValue: ''}),
  author: DS.attr(),
  speaker: DS.attr('string'),
  image: DS.attr('string'),
  fileUrl: DS.attr('string'),
  fileSize: DS.attr('string'),
  updated_at: DS.attr('date'),
  created_at: DS.attr('date'),
  status: DS.attr('string', { defaultValue: 'draft'}),
  tags: DS.attr(),
});
